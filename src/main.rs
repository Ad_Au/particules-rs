// #![windows_subsystem = "windows"]

use amethyst::ecs::prelude::{Component, VecStorage};
use amethyst::{
    assets::AssetLoaderSystemData,
    core::{nalgebra::Vector3, Transform, TransformBundle},
    ecs::{Entities, Join, System, WriteStorage},
    prelude::*,
    renderer::*,
};
use amethyst_utils::fps_counter::FPSCounter;
use amethyst_utils::fps_counter::FPSCounterBundle;
use rand::random;

pub struct SimulationState;

impl SimpleState for SimulationState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        let mat_defaults = world.read_resource::<MaterialDefaults>().0.clone();

        let (mesh, albedo) = {
            let mesh = world.exec(|loader: AssetLoaderSystemData<'_, Mesh>| {
                loader.load_from_data(
                    Shape::Sphere(32, 32).generate::<Vec<PosNormTangTex>>(None),
                    (),
                )
            });
            let albedo = world.exec(|loader: AssetLoaderSystemData<'_, Texture>| {
                loader.load_from_data([1.0, 1.0, 1.0, 1.0].into(), ())
            });

            (mesh, albedo)
        };

        let roughness = 0.0f32;
        let metallic = 0.5f32;

        let mut pos = Transform::default();
        pos.set_scale(0.1, 0.1, 0.1);

        let metallic = [metallic, metallic, metallic, 1.0].into();
        let roughness = [roughness, roughness, roughness, 1.0].into();

        let (metallic, roughness) = world.exec(|loader: AssetLoaderSystemData<'_, Texture>| {
            (
                loader.load_from_data(metallic, ()),
                loader.load_from_data(roughness, ()),
            )
        });

        let mtl = Material {
            albedo: albedo.clone(),
            metallic,
            roughness,
            ..mat_defaults.clone()
        };

        for _ in 0..128 {
            pos.set_xyz(
                random::<f32>() - 0.5,
                random::<f32>() - 0.5,
                random::<f32>() - 0.5,
            );

            world
                .create_entity()
                .with(mesh.clone())
                .with(mtl.clone())
                .with(pos.clone())
                .with(Velocity::new())
                .with(Acceleration::new())
                .build();
        }

        let light1: Light = PointLight {
            intensity: 6.0,
            color: [0.8, 0.0, 0.0].into(),
            ..PointLight::default()
        }
        .into();

        let mut light1_transform = Transform::default();
        light1_transform.set_xyz(6.0, 6.0, -6.0);

        let light2: Light = PointLight {
            intensity: 5.0,
            color: [0.0, 0.3, 0.7].into(),
            ..PointLight::default()
        }
        .into();

        let mut light2_transform = Transform::default();
        light2_transform.set_xyz(6.0, -6.0, -6.0);

        let light3: Light = PointLight {
            intensity: 8.0,
            color: [1.0, 1.0, 1.0].into(),
            ..PointLight::default()
        }
        .into();

        let light3_transform = Transform::default();

        world
            .create_entity()
            .with(light1)
            .with(light1_transform)
            .build();

        world
            .create_entity()
            .with(light2)
            .with(light2_transform)
            .build();

        world
            .create_entity()
            .with(light3)
            .with(light3_transform)
            .build();

        let mut transform = Transform::default();
        transform.set_xyz(0.0, 0.0, -5.0);
        transform.rotate_local(Vector3::y_axis(), std::f32::consts::PI);

        world
            .create_entity()
            .with(Camera::from(Projection::perspective(
                1.3,
                std::f32::consts::FRAC_PI_3,
            )))
            .with(transform)
            .build();
    }

    fn fixed_update(&mut self, state_data: StateData<GameData>) -> SimpleTrans {
        let world = state_data.world;
        let fps = world.read_resource::<FPSCounter>().sampled_fps();
        println!("{}", fps);

        Trans::None
    }
}

pub struct Velocity {
    v: Vector3<f32>,
}

impl Velocity {
    fn new() -> Self {
        Velocity {
            v: Vector3::<f32>::zeros(),
        }
    }
}

impl Component for Velocity {
    type Storage = VecStorage<Self>;
}

pub struct Acceleration {
    a: Vector3<f32>,
}

impl Acceleration {
    fn new() -> Self {
        Acceleration {
            a: Vector3::<f32>::zeros(),
        }
    }
}

impl Component for Acceleration {
    type Storage = VecStorage<Self>;
}

pub struct GravitationSystem;

impl<'s> System<'s> for GravitationSystem {
    type SystemData = (
        WriteStorage<'s, Acceleration>,
        WriteStorage<'s, Velocity>,
        WriteStorage<'s, Transform>,
        Entities<'s>,
    );

    fn run(
        &mut self,
        (mut accelerations, mut velocities, mut transforms, entities): Self::SystemData,
    ) {
        for c_e in (&*entities).join() {
            for d_e in (&*entities).join() {
                if let Some(mut c_acc) = accelerations.get_mut(c_e) {
                    if c_e != d_e {
                        let v = transforms.get(d_e).unwrap().translation()
                            - transforms.get(c_e).unwrap().translation();
                        let n = v.normalize() / v.norm();
                        c_acc.a += n;
                    }
                    c_acc.a *= 0.00000005;
                    let c_vel = velocities.get_mut(c_e).unwrap();
                    c_vel.v += c_acc.a;
                    transforms.get_mut(c_e).unwrap().move_global(c_vel.v);
                    c_acc.a = Vector3::zeros();
                }
            }
        }
    }
}

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let path = "./resources/display_config.ron";

    let game_data = GameDataBuilder::default()
        .with_basic_renderer(path, DrawPbm::<PosNormTangTex>::new(), false)?
        .with_bundle(TransformBundle::new())?
        .with_bundle(FPSCounterBundle::default())?
        .with(GravitationSystem, "gravitation_system", &[]);

    let mut game = Application::new("./", SimulationState, game_data)?;
    game.run();
    Ok(())
}
